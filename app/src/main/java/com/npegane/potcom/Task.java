package com.npegane.potcom;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

/**
 * Created by Pegane on 05/02/2018.
 */

public class Task {
    public int id;
    public String name;
    public Double price;
    public String status;
    public String user;

    public Task(){

    }

    public Task(String name, Double price ,String status, String user, String eventId,String eventType){
        this.name = name;
        this.price = price;
        this.user = user;
        this.status = status;
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int value = (int) dataSnapshot.child("task").getChildrenCount();
                id = value;//.getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Fail");
            }
        });
        UUID uniqueKey = UUID.randomUUID();
        String uniqueID = uniqueKey.toString();
        mDatabase.child("Event").child(eventType).child(eventId).child("eventTasks").child(uniqueID).setValue(this);
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(Double price){
        this.price = price;
    }

    public void setStatus(String status){
        this.status = status;
    }
}
