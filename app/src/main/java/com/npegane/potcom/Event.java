package com.npegane.potcom;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by VS on 11/02/2018.
 */

public abstract class Event {
    public String nameEvent;
    public String type;
    public String budget;
    public String description;
    public Event event;
    public ArrayList<User> userList;
    public ArrayList<Task> taskList;

    public Event() {}
    public Event(String nameEvent, String type, String typeBudget, String budget, String description, ArrayList<User> userList) {
        this.nameEvent = nameEvent;
        this.type = type;
        this.budget = budget;
        this.description = description;
        this.userList = userList;
        this.taskList = new ArrayList<Task>();
    }
    public abstract void addEvent(String uniqueKey, String mainPseudo);
    public void addTask(Task task){
        taskList.add(task);
    }

    public String getNameEvent() {
        return nameEvent;
    }

    public void setNameEvent(String nameEvent) {
        this.nameEvent = nameEvent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
