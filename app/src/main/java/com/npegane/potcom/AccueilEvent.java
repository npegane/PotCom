package com.npegane.potcom;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class AccueilEvent extends AppCompatActivity {

    private Button createEventBtn;
    private String mainPseudo;
    private ArrayList<String> userEventList;

    int j = 0;
    int i = 0;

    private ArrayList<String> listEvent = new ArrayList<String>();
    private ArrayAdapter<String> adapterPonctuel;
    private ArrayAdapter<String> adapterQuotidien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                mainPseudo = null;
            } else {
                mainPseudo = extras.getString("PSEUDO");
            }
        } else {
            mainPseudo = (String) savedInstanceState.getSerializable("PSEUDO");
        }
        createEventBtn = (Button) findViewById(R.id.create_btn);
        userEventList = new ArrayList<String>();
        getEvent("Ponctuel", mainPseudo);
        getEvent("Quotidien", mainPseudo);


        createEventBtn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AccueilEvent.this, CreateEvent.class);
                intent.putExtra("PSEUDO", mainPseudo);
                startActivity(intent);
            }
        });

    }

    public void getEvent(final String typeEvent, final String mainPseudo) {
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final ArrayList<String> tabEventId = new ArrayList<String>();


        DatabaseReference eventPonctuel = mDatabase.child("Participation")
                .child(typeEvent)
                .child("All User")
                .child(mainPseudo);
        System.out.println(eventPonctuel);

        eventPonctuel.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                if (map.keySet() != null) {
                    final String[] value = map.keySet().toArray(new String[map.size()]);
                    String nbValue = String.valueOf(value.length);

                    for ( i = 0; i < value.length; i++) {

                        DatabaseReference getEventPonctuel = mDatabase.child("Event")
                                .child(typeEvent)
                                .child(value[i]);

                        getEventPonctuel.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Ponctuel eventPonctuel = dataSnapshot.getValue(Ponctuel.class);

                                if (eventPonctuel != null) {
                                    listEvent.add(eventPonctuel.getNameEvent());
                                    if (i - 1 < value.length) {
                                        adapterPonctuel = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, listEvent);
                                        ListView listPonctuel = (ListView) findViewById(R.id.list_evnt);
                                        //System.out.println(value[i]);
                                        listPonctuel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                Intent intent = new Intent(AccueilEvent.this, ActivityTasklist.class);
                                                intent.putExtra("PSEUDO", mainPseudo);
                                                intent.putExtra("EVENT_ID",value[i-1]);
                                                intent.putExtra("EVENT_TYPE",typeEvent);
                                                startActivity(intent);
                                            }
                                        });
                                        listPonctuel.setAdapter(adapterPonctuel);
                                    }
                                    if (typeEvent == "Quotidien") {
                                        adapterQuotidien = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, listEvent);
                                        ListView listPonctuel = (ListView) findViewById(R.id.list_quotidien);
                                        listPonctuel.setAdapter(adapterQuotidien);
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                    listEvent.clear();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
