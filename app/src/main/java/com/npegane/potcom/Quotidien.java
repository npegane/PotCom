package com.npegane.potcom;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by VS on 10/02/2018.
 */

public class Quotidien extends Event {

    public String typeBudget;

    public Quotidien(){}

    public Quotidien(String nameEvent, String type, String typeBudget, String budget, String description, ArrayList<User> userList){
        super(nameEvent, type, typeBudget, budget, description, userList);
        this.typeBudget = typeBudget;
    }

    public String getTypeBudget() {
        return typeBudget;
    }

    public void setTypeBudget(String typeBudget) {
        this.typeBudget = typeBudget;
    }

    public void addEvent(String uniqueKey, String mainPseudo) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("Event")
                .child(this.type)
                .child(uniqueKey)
                .setValue(this);

        mDatabase.child("Participation")
                .child(this.type)
                .child("All User")
                .child(mainPseudo)
                .child(uniqueKey)
                .setValue("true");

        mDatabase.child("Participation")
                .child(this.type)
                .child("All Event")
                .child(uniqueKey)
                .child(mainPseudo)
                .setValue("true");
    }

}
