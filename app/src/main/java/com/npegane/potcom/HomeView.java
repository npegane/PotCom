package com.npegane.potcom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class HomeView extends AppCompatActivity {

    private Button btnCreate;
    private Button btnConnexion;
    private EditText editTextPseudo;
    private EditText editTextPassword;
    private TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_view);

        editTextPseudo = (EditText) findViewById(R.id.editTextPseudo);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        txtMessage = (TextView) findViewById(R.id.txtMessage);

        btnCreate = (Button) findViewById(R.id.btnCreate);
        btnConnexion = (Button) findViewById(R.id.btnConnexion);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeView.this, CreateAccount.class);
                startActivity(intent);
            }
        });

        btnConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();

                //Création d'une référence à l'objet Users Pour récupérer le mot de passe
                DatabaseReference getPassword = mDatabase.child("users")
                        .child(editTextPseudo.getText().toString()) //récuperation du bon user
                        // avec le champ Pseudo utilisé pour l'Id
                        .child("password");//On récupere l'objet password du User (pas la valeur)

                getPassword.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String password = dataSnapshot.getValue(String.class); //recupération de la valeur du mot de passe
                        if (password.equals(editTextPassword.getText().toString())) {
                            txtMessage.setTextColor(Color.GREEN);
                            txtMessage.setText("Connexion réussie");
                            String pseudo = editTextPseudo.getText().toString();
                            Intent intent = new Intent(HomeView.this, AccueilEvent.class);
                            System.out.println(pseudo);
                            intent.putExtra("PSEUDO",pseudo);
                            startActivity(intent);
                        } else {
                            txtMessage.setTextColor(Color.RED);
                            txtMessage.setText("Mauvais mot de passe");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                /*mDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        System.out.println(dataSnapshot.child("users").child("2"));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("Fail");
                    }
                });*/
                //System.out.println(mDatabase.child(editTextPseudo.getText().toString()).child("password").getKey());

            }
        });
    }
}
