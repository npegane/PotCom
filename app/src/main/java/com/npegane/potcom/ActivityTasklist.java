package com.npegane.potcom;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActivityTasklist extends AppCompatActivity {
    private String user;
    private String name;
    private String status ;
    private Double price ;
    private String eventId;
    private String eventType;
    private TextView textNom;
    private TextView textTypeBudget;
    private TextView textBudget;
    private EditText nameEdit;
    private EditText pseudoEdit;
    private EditText statusEdit;
    private EditText priceEdit;
    private TableLayout taskTab;
    private String mainPseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_task);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                System.out.println("NULL");
                mainPseudo= null;
                eventId = null;
                eventType = null;
            } else {
                System.out.println("NOT NULL");
                mainPseudo= extras.getString("PSEUDO");
                eventType = extras.getString("EVENT_TYPE");
                eventId = extras.getString("EVENT_ID");
            }
        } else {
            System.out.println("SERIALIZABLE");
            mainPseudo= (String) savedInstanceState.getSerializable("PSEUDO");
            eventType = (String) savedInstanceState.getSerializable("EVENT_TYPE");
            eventId = (String) savedInstanceState.getSerializable("EVENT_ID");
        }

        nameEdit = (EditText)findViewById(R.id.EditViexCol1);
        //nameEdit.setText(mainPseudo);

        priceEdit = (EditText)findViewById(R.id.EditViexCol2);

        pseudoEdit = (EditText)findViewById(R.id.EditViexCol3);
        taskTab = (TableLayout) findViewById(R.id.taskTab);


        textNom = (TextView) findViewById(R.id.textNom);
        textTypeBudget = (TextView) findViewById(R.id.textTypeBudget);
        textBudget = (TextView) findViewById(R.id.textBudget);
        loadTaskList();

        final Button button = (Button) findViewById(R.id.buttonAdd);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                name = nameEdit.getText().toString();
                user = pseudoEdit.getText().toString();
                price = Double.valueOf(priceEdit.getText().toString());
                Task task = new Task(name, price, "attente", user, eventId,eventType);
            }
        });

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference("Event");
        DatabaseReference getEvent =  mDatabase.child(eventType).child(eventId);
        getEvent.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Event event;
                if(eventType == "Ponctuel"){
                    event = dataSnapshot.getValue(Ponctuel.class);
                } else {
                    event = dataSnapshot.getValue(Quotidien.class);
                }
                textNom.setText(event.nameEvent);
                textBudget.setText(event.budget);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void loadTaskList(){
        //Récupérer les users Depuis Firebase
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference("Event").child(eventType).child(eventId).child("eventTasks");

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final Task task = dataSnapshot.getValue(Task.class);
                System.out.println("Name:" + task.name);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(1, 0,0, 0);
                TextView txtName = new TextView(getBaseContext());
                txtName.setText(task.name);
                //txtName.setLayoutParams(params);

                TextView txtPrice = new TextView(getBaseContext());
                txtPrice.setText(String.valueOf(task.price));
                //txtPrice.setLayoutParams(params);

                TextView txtPseudo = new TextView(getBaseContext());
                txtPseudo.setText(task.user);
                //txtPseudo.setLayoutParams(params);

                TableRow rowTask = new TableRow(getBaseContext());
                rowTask.addView(txtName);
                rowTask.addView(txtPrice);
                rowTask.addView(txtPseudo);
                taskTab.addView(rowTask);
                /*ImageView user_icon = new ImageView(getBaseContext());
                user_icon.setImageResource(R.mipmap.user);
                user_icon.setMaxWidth(5);
                user_icon.setMaxHeight(5);
                user_layout.addView(user_icon);*/
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
