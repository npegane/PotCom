package com.npegane.potcom;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.opengl.Visibility;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateAccount extends AppCompatActivity {
    private ImageView imgCalendar;
    private EditText txtDate;
    private DatePickerDialog datePickerDialog;
    private Button btnCreate;
    private EditText txtPseudo;
    private EditText txtName;
    private EditText txtFirstName;
    private EditText txtEmail;
    private EditText txtCity;
    private EditText txtPhone;
    private EditText txtPassword;
    private EditText txtPasswordConfirm;

    private boolean name            = false;
    private boolean firstname       = false;
    private boolean email           = false;
    private boolean date            = false;
    private boolean city            = false;
    private boolean phone           = false;
    private boolean password        = false;
    private boolean passwordConfirm = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        imgCalendar = (ImageView) findViewById(R.id.imgCalendar);
        txtPseudo = (EditText) findViewById(R.id.txtPseudo);
        txtName = (EditText) findViewById(R.id.txtName);
        txtFirstName = (EditText) findViewById(R.id.txtFirstname);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtDate =(EditText) findViewById(R.id.txtDate);
        txtCity = (EditText) findViewById((R.id.txtCity));
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnCreate = (Button) findViewById(R.id.btnCreate);
        txtPasswordConfirm = (EditText) findViewById(R.id.txtPasswordConfirm);

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        Activity a = CreateAccount.this;
        this.datePickerDialog = new DatePickerDialog(a,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        monthOfYear++;
                        String txt = dayOfMonth+"/"+monthOfYear+"/"+year;
                        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
                        try {
                            Date date = df.parse(txt);
                            txt = df.format(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        txtDate.setText(txt);
                    }
                }, mYear, mMonth, mDay);

        imgCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (txtName.getText().toString() != "") {
                    name = true;
                } else {
                    txtName.setText("Invalid");
                }

                if (txtFirstName.getText().toString() != "") {
                    firstname = true;
                } else {
                    txtFirstName.setText("Invalid");
                }

                if (txtDate.getText().toString() != "") {
                    date = true;
                } else {
                    txtDate.setText("Invalid");
                }

                if (txtCity.getText().toString() != "") {
                    city = true;
                } else {
                    txtCity.setText("Invalid");
                }

                if (txtEmail.getText().toString() != "") {
                    email = true;

                    if (isEmailValid(txtEmail.getText().toString())) {
                        Log.d("email verified : ", txtEmail.getText().toString());
                    } else {
                        email = false;
                        txtEmail.setText("Invalid");
                    }
                } else {
                    txtEmail.setText("Invalid");
                }

                if (txtPhone.getText().toString() != "" ){
                    phone = true;
                } else {
                    txtPhone.setText("Invalid");
                }

                if (txtPassword.getText().toString() != ""
                        && txtPassword.getText().toString()
                        .equals(txtPasswordConfirm.getText().toString())) {
                    password = true;
                    Log.d("Password : ",txtPassword.getText().toString());
                    Log.d("Password confirm : ",txtPasswordConfirm.getText().toString());
                } else {
                    txtPassword.setText("Invalid");
                }

                if (name && firstname && city && date && phone && password && email) {
                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    DatabaseReference ref = database.getReference("server/saving-data/fireblog/posts");
                    User newUser = new User(txtPseudo.getText().toString(),
                            txtName.getText().toString(),
                            txtFirstName.getText().toString(),
                            txtDate.getText().toString(),
                            txtCity.getText().toString(),
                            txtEmail.getText().toString(),
                            txtPhone.getText().toString(),
                            txtPassword.getText().toString());

                    Intent intent = new Intent(CreateAccount.this, HomeView.class);
                    startActivity(intent);

                    Toast.makeText(getBaseContext(), "Votre compte a bien été créé", Toast.LENGTH_LONG).show();

                    Log.d("Informations valide","True");
                }


            }

            /**
             * method is used for checking valid email id format.
             *
             * @param email
             * @return boolean true for valid false for invalid
             */
            public  boolean isEmailValid(String email) {
                String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(email);
                return matcher.matches();
            }
        });
    }
}
