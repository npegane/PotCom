package com.npegane.potcom;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Pegane on 11/02/2018.
 */

public class CreateEvent extends AppCompatActivity {
    private Button createBtn;
    private EditText nameEvent;
    private Spinner type;
    private Spinner typeBudget;
    private EditText budget;
    private EditText description;
    private LinearLayout layoutTypeEvent;
    private Event event;
    private ArrayList<User> userList;
    private ArrayList<User> addedUsers;
    private LinearLayout ll_userlist;
    private String mainPseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                mainPseudo= null;
            } else {
                mainPseudo= extras.getString("PSEUDO");
            }
        } else {
            mainPseudo= (String) savedInstanceState.getSerializable("PSEUDO");
        }
        createBtn = (Button) findViewById(R.id.create_btn);
        nameEvent = (EditText)findViewById(R.id.name_event);
        type = (Spinner)findViewById(R.id.spinner);
        typeBudget = (Spinner)findViewById(R.id.spinnerBudget);
        budget = (EditText)findViewById(R.id.budget_event);
        description = (EditText)findViewById(R.id.description);
        layoutTypeEvent = (LinearLayout) findViewById(R.id.layoutTypeEvent);
        ll_userlist = (LinearLayout) findViewById(R.id.ll_userlist);
        userList = new ArrayList<User>();
        addedUsers = new ArrayList<User>();


        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItem().toString().equals("Quotidien")){
                    layoutTypeEvent.setVisibility(View.VISIBLE);
                } else {
                    layoutTypeEvent.setVisibility(View.INVISIBLE );
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        loadUserList();
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(type.getSelectedItem().toString().equals("Ponctuel")){
                    System.out.println(type.getSelectedItem().toString());
                    System.out.println(nameEvent.getText().toString());
                    event = new Ponctuel(
                            nameEvent.getText().toString(),
                            type.getSelectedItem().toString(),
                            "simple",
                            budget.getText().toString(),
                            description.getText().toString(),
                            addedUsers
                    );
                }
                else if(type.getSelectedItem().toString().equals("Quotidien")){
                    event = new Quotidien(
                            nameEvent.getText().toString(),
                            type.getSelectedItem().toString(),
                            typeBudget.getSelectedItem().toString(),
                            budget.getText().toString(),
                            description.getText().toString(),
                            addedUsers
                    );
                }
                UUID uniqueKey = UUID.randomUUID();
                String uniqueID = uniqueKey.toString();

                event.addEvent(uniqueID, mainPseudo);

                //Event new_event = new Event(nameEvent.getText().toString(),type.getSelectedItem().toString(),typeBudget.getSelectedItem().toString(),budget.getText().toString(),description.getText().toString());
                Intent intent = new Intent(CreateEvent.this, AccueilEvent.class);
                intent.putExtra("PSEUDO",mainPseudo);
                startActivity(intent);

                Toast.makeText(getBaseContext(), "Evénement ajouté", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void loadUserList(){
        //Récupérer les users Depuis Firebase
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final User user = dataSnapshot.getValue(User.class);
                userList.add(user);
                LinearLayout user_layout = new LinearLayout(getBaseContext());

                user_layout.setOrientation(LinearLayout.HORIZONTAL);
                user_layout.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                /*ImageView user_icon = new ImageView(getBaseContext());
                user_icon.setImageResource(R.mipmap.user);
                user_icon.setMaxWidth(5);
                user_icon.setMaxHeight(5);
                user_layout.addView(user_icon);*/
                final CheckedTextView txtUser = new CheckedTextView(getBaseContext());
                txtUser.setChecked(false);
                txtUser.setCheckMarkDrawable(android.R.drawable.ic_input_add);
                txtUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println(txtUser.isChecked());
                        txtUser.setChecked(!(txtUser.isChecked()));
                        txtUser.setCheckMarkDrawable(txtUser.isChecked() ? android.R.drawable.ic_delete : android.R.drawable.ic_input_add);
                        if(!txtUser.isChecked()){
                            addedUsers.remove(user);
                        } else {
                            addedUsers.add(user);
                        }
                    }
                });
                txtUser.setText(user.name+" "+user.lastName);

                user_layout.addView(txtUser);
                ll_userlist.addView(user_layout);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
