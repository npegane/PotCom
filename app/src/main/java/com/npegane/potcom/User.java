package com.npegane.potcom;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
/**
 * Created by VS on 29/01/2018.
 */

public class User {
    //public static int id;
    public String pseudo;
    public String lastName;
    public String name;
    public String birthDate;
    public String city;
    public String phoneNumber;
    public String password;
    public String username;
    public String email;
    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String pseudo, String name, String lastName, String birthDate, String city, String email, String phoneNumber, String password) {
        this.pseudo = pseudo;
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.city = city;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //RECUPERER LE DERNIER ID
        /*mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int value = (int) dataSnapshot.child("users").getChildrenCount();
                id = value;//.getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Fail");
            }
        });*/
        mDatabase.child("users").child(pseudo).setValue(this);
        //System.out.println("users");
        //System.out.println(mDatabase.child("users").child("1"));
    }


    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

}
