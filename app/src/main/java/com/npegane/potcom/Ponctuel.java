package com.npegane.potcom;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by VS on 10/02/2018.
 */

public class Ponctuel extends Event{

    public String date;
    public String duree;
    public String lieu;

    //public Ponctuel(String s, String toString, String simple, String string, String s1){};
    public Ponctuel(String nameEvent, String type, String typeBudget, String budget, String description, ArrayList<User> userList){
        super(nameEvent, type, typeBudget, budget, description,userList);
    }
    public Ponctuel(String nameEvent, String type, String typeBudget, String budget, String description, ArrayList<User> userList, String date, String duree, String lieu){
        super(nameEvent, type, typeBudget, budget, description, userList);
        this.date = date;
        this.duree = duree;
        this.lieu = lieu;
    }
    public Ponctuel(){};
    public Ponctuel(String nameEvent, String type, String typeBudget, String budget, String description, ArrayList<User> userList, String date){
        super(nameEvent, type, typeBudget, budget, description, userList);



    //public Ponctuel(String s, String toString, String simple, String string, String s1){};
        this.date = date;
        this.duree = duree;
        this.lieu = lieu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public String toString(){
        return ("Nom : "+nameEvent+"\n"+"Type : "+type+"\n"+"Budget : "+budget+"\n");
    }

    public void addEvent(String uniqueKey, String mainPseudo) {
        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("Event")
                .child(this.type)
                .child(uniqueKey)
                .setValue(this);

        mDatabase.child("Participation")
                .child(this.type)
                .child("All User")
                .child(mainPseudo)
                .child(uniqueKey)
                .setValue("true");

        mDatabase.child("Participation")
                .child(this.type)
                .child("All Event")
                .child(uniqueKey)
                .child(mainPseudo)
                .setValue("true");
    }
}
